<?php
require '../config/conn.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $response = [];

    $id = $_POST['id'];
    $namaLengkap = $_POST['namaLengkap'];
    $nisn = $_POST['nisn'];
    $tempatLahir = $_POST['tempatLahir'];
    $tanggalLahir = $_POST['tanggalLahir'];
    $alamat = $_POST['alamat'];
    $changePhoto = $_POST['changePhoto'];
    $password = $_POST['password'];

    $allowedFormats = ['image/jpeg', 'image/jpg', 'image/png'];
    if (isset($_FILES['gambar']) && in_array($_FILES['gambar']['type'], $allowedFormats)) {
        $gambarPath = '../gambar/' . $_FILES['gambar']['name'];
        if (move_uploaded_file($_FILES['gambar']['tmp_name'], $gambarPath)) {
            $insert = mysqli_query($conn, "UPDATE siswa SET namaLengkap = '$namaLengkap', nisn = '$nisn', tempatLahir = '$tempatLahir',
            tanggalLahir = '$tanggalLahir', alamat = '$alamat', gambar = '$gambarPath', changePhoto = '$changePhoto', password = '$password' WHERE id = '$id'");

            $response['value'] = 1;
            $response['success'] = true;
            $response['message'] = "Data berhasil diubah.";
        } else {
            $response['value'] = 0;
            $response['success'] = false;
            $response['message'] = "Gagal mengunggah gambar.";
        }
    } else {
        $response['value'] = 0;
        $response['success'] = false;
        $response['message'] = "Format gambar tidak diizinkan. Hanya PNG, JPG, dan JPEG yang diperbolehkan.";
    }

    echo json_encode($response);
} else {
    $response['value'] = 0;
    $response['success'] = false;
    $response['message'] = "Metode permintaan tidak valid.";
    echo json_encode($response);
}
