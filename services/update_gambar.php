<?php
require '../config/conn.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $response = array();
    $id = $_POST['id'];
    $gambar = $_FILES['gambar'];
    $allowedFormats = ['jpg', 'jpeg', 'png'];
    $maxFileSize = 2 * 1024 * 1024; // Ukuran maksimum file (2MB)

    if ($gambar['error'] === UPLOAD_ERR_OK) {
        $gambarName = $gambar['name'];
        $gambarSize = $gambar['size'];
        $gambarTmpName = $gambar['tmp_name'];
        $gambarType = strtolower(pathinfo($gambarName, PATHINFO_EXTENSION));

        if (in_array($gambarType, $allowedFormats)) {
            if ($gambarSize <= $maxFileSize) {
                $gambarFileName = uniqid() . '_' . $gambarName;
                $uploadPath = '../gambar/' . $gambarFileName;
                if (move_uploaded_file($gambarTmpName, $uploadPath)) {
                    $update = "UPDATE siswa SET gambar = '$gambarFileName' WHERE id = $id";
                    if (mysqli_query($conn, $update)) {
                        $response['value'] = 1;
                        $response['message'] = "Gambar berhasil diunggah dan data berhasil diperbarui";
                    } else {
                        $response['value'] = 0;
                        $response['message'] = "Gambar berhasil diunggah tetapi terjadi masalah dalam memperbarui data";
                    }
                } else {
                    $response['value'] = 0;
                    $response['message'] = "Gagal mengunggah gambar";
                }
            } else {
                $response['value'] = 0;
                $response['message'] = "Ukuran gambar terlalu besar (maksimum 2MB)";
            }
        } else {
            $response['value'] = 0;
            $response['message'] = "Format gambar tidak valid. Hanya diperbolehkan JPG, JPEG, atau PNG.";
        }
    } else {
        $response['value'] = 0;
        $response['message'] = "Terjadi kesalahan saat mengunggah gambar";
    }

    echo json_encode($response);
}
