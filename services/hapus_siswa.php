<?php
require '../config/conn.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $response = [];

    $id = $_POST['id'];

    $query = "DELETE FROM siswa WHERE id = '$id'";
    $result = mysqli_query($conn, $query);

    if ($result) {
        $response['success'] = true;
        $response['message'] = "Data berhasil dihapus.";
    } else {
        $response['success'] = false;
        $response['message'] = "Gagal menghapus data: " . mysqli_error($conn);
    }

    echo json_encode($response);
} else {
    $response['success'] = false;
    $response['message'] = "Metode permintaan tidak valid.";
    echo json_encode($response);
}
